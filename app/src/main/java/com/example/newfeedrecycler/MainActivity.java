package com.example.newfeedrecycler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.example.newfeedrecycler.adapter.NewFeedAdapter;
import com.example.newfeedrecycler.adapter.NewFeedListener;
import com.example.newfeedrecycler.model.Article;
import com.example.newfeedrecycler.model.DataSource;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NewFeedListener {

    private RecyclerView recyclerView;
    private ArrayList<Article> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getdata;
        list = DataSource.getDataSource();

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        NewFeedAdapter adapter = new NewFeedAdapter(this);
        adapter.setListener(this);
        adapter.addMoreItems(list);//get data;
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onNewFeedClick(int position) {
        Toast.makeText(this, list.get(position).getTitle(), Toast.LENGTH_SHORT).show();
    }
}
