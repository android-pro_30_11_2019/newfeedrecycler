package com.example.newfeedrecycler.adapter;

import android.content.Context;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.newfeedrecycler.R;
import com.example.newfeedrecycler.model.Article;

public class NewFeedViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    private TextView tvTittle,tvdate,tvViews;
    private ImageView imageView;
    private NewFeedListener listener;

    public NewFeedViewHolder(@NonNull View itemView, Context context, final NewFeedListener listener) {
        super(itemView);
        tvTittle = itemView.findViewById(R.id.tvTittle);
        tvdate = itemView.findViewById(R.id.tvdate);
        tvViews = itemView.findViewById(R.id.tvViews);
        imageView = itemView.findViewById(R.id.ivthumbnail);
        this.context = context;
        this.listener = listener;

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNewFeedClick(getAdapterPosition());
            }
        });
    }

    public void onBind(Article article) {
        tvTittle.setText(article.getTitle());
        tvdate.setText(article.getPostDate());
        tvViews.setText(article.getView()+" Views");


        Log.e("eeeeeee",article.getImageUrl());
        Glide.with(context)
                .load(article.getImageUrl())
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .error(R.drawable.ic_do_not_disturb_on_black_24dp)
                .into(imageView);
    }
}
