package com.example.newfeedrecycler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newfeedrecycler.R;
import com.example.newfeedrecycler.model.Article;
import com.example.newfeedrecycler.model.DataSource;

import java.util.ArrayList;
import java.util.List;

public class NewFeedAdapter extends RecyclerView.Adapter<NewFeedViewHolder> {
    //Datasouce;
    private List<Article> articles;
    private Context context;
    private NewFeedListener listener;

    public NewFeedAdapter(Context context, NewFeedListener listener) {
        this.articles = new ArrayList<>();
        this.context = context;
        this.listener = listener;
    }
    public NewFeedAdapter(Context context) {
        this.articles = new ArrayList<>();
        this.context = context;
    }

    public void setListener(NewFeedListener listener) {
        this.listener = listener;
    }

    public void addMoreItems(List<Article> articles){
        this.articles.addAll(articles);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NewFeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_new_feed, parent, false);
        return new NewFeedViewHolder(itemView,context,listener);

    }

    @Override
    public void onBindViewHolder(@NonNull NewFeedViewHolder holder, int position) {
        holder.onBind(articles.get(position));
    }

    @Override
    public int getItemCount() {
        return this.articles.size();
    }
}
