package com.example.newfeedrecycler.adapter;

public interface NewFeedListener {
    void onNewFeedClick(int position);
}
