package com.example.newfeedrecycler.model;

public class Article {
    private int id;
    private String title;
    private int View;
    private String postDate;
    private String imageUrl;

    public Article(int id, String title, int view, String postDate, String imageUrl) {
        this.id = id;
        this.title = title;
        View = view;
        this.postDate = postDate;
        this.imageUrl = imageUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getView() {
        return View;
    }

    public void setView(int view) {
        View = view;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
